<?php require_once('includes/header.php');
require_once('table.php');
$table = new table();
?>
    <section class="img-bg" style="background-image: url('img/bg-wine-final.jpeg')">
        <h1 class="fade-in-up">Wine similarity</h1>
    </section>
    <section class="bg-gray database-input">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2 text-center fade-in-up">
                    <div class="text-area-wrapper">
                        <h2>1. Add your favourite wine</h2>
                        <p class="subheading">Want to add your wine to our database? Great. Simply paste in the
                            url from vivino.com</p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-10 col-md-offset-1 text-center fade-in-up">
                    <textarea placeholder="Insert links to your favourite wine (separated by space)"
                              id="text-area-url" rows="10"
                              cols="100"></textarea>
                    <div><a class="btn url" data-type="url">Insert urls</a>
                        <div class="load-outer-wrapper-url">
                            <div class="load-wrapper">
                                <div class="loader"></div>
                            </div>
                        </div>
                        <p class="response-message subheading"></p></div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section class="insert-sitmap-url">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2 text-center fade-in-up">
                    <h2>2. Add sitemap</h2>
                    <p class="subheading">Have you a sitemap containing your favourite wines? Paste in the url, and we
                        will loop through the sitemap and insert them, if they do not already exists</p>
                </div>
                <div class="col-sm-12 col-md-10 col-md-offset-1 text-center fade-in-up">
                    <textarea id="text-area-sitemap"
                              placeholder="Insert links to a sitemap with urls of your favourite wine"
                              class="text-area" rows="10"
                              cols="100"></textarea>
                    <div><a class="btn sitemap" data-type="sitemap">Insert sitemap</a>
                        <div class="load-outer-wrapper-sitemap">
                            <div class="load-wrapper">
                                <div class="loader"></div>
                            </div>
                        </div>
                        <p class="response-message-sitemap subheading"></p></div>
                </div>
            </div>
        </div>
    </section>
    <section class="show-table bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center fade-in-up">
                    <h2>3. Initial Database</h2>
                    <p class="subheading">The initial database scraped from vivino.com with
                        <?php echo $table->table_count_initial(); ?> wines. To model the data, the data first needs to
                        be prepared.</p>
                    <?php
                    $table->show_table_initial();
                    ?>
                </div>
            </div>
        </div>
    </section>
    <section class="show-table">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center fade-in-up">
                    <h2>4. Prepared Database</h2>
                    <p class="subheading">The database has been limited to <?php echo $table->table_count(); ?> wines
                        and the attributes: wineyard,
                        alcohol and region style has been removed. Furthermore, four attributes (country, region, grapes
                        and type) has been converted into numericals.
                        .</p>
                    <?php $table->show_table_prepared(); ?>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-gray">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2 text-center fade-in-up">
                    <h2>5. Explore similar wines</h2>
                    <p class="subheading">Choose your favourite wine and explore similar ones. When you select a wine,
                        we apply the Jaccard method and loops through the sample dataset
                        of <?php echo $table->table_count(); ?> wines. Thereafter, we provide you with similar wines
                        ranked by price and user feedback.
                    </p>
                </div>
                <div class="col-sm-12 col-md-10 col-md-offset-1 text-center">
                    <?php $table->select_wine(); ?>
                    <div class="row" id="similar-wines"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="img-bg" style="background-image: url('img/cheers.jpg')">
        <div class="img-wrapper">
            <h2 class="fade-in-up">6. Taste it yourself. Cheers</h2>
            <img class="fade-in-up" src="img/glasses-with-wine.svg">
        </div>
    </section>
<?php require_once('includes/footer.php'); ?>