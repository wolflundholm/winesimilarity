<?php

require_once('config.php');
require_once('scraper.php');

class table
{
    public $link;

    public function __construct()
    {
        $db_connection = new config();
        $this->link = $db_connection->dbConnection();
        return $this->link;
    }

    public function table_count()
    {
        $query = "SELECT * FROM wine_v1";
        $rows = $this->link->query($query)->rowCount();
        return $rows;
    }

    public function table_count_initial()
    {
        $query = "SELECT * FROM wine";
        $rows = $this->link->query($query)->rowCount();
        return $rows;
    }

    public function show_table_initial()
    {
        $query = "SELECT * FROM wine ORDER BY score DESC, score DESC";
        $rows = $this->link->query($query);

        echo "<table class='table table-striped'><thead>
        <th>Name</th>
        <th>Score</th>
        <th>Reviews</th>
        <th>Country</th>    
        <th>Region</th>
        <th>Region style</th>
        <th>Wineyard</th>
        <th>Alcohol</th>
        <th>Grapes</th>
        <th>Price ($)</th>
        ";
        echo "</thead><tbody>";
        foreach ($rows as $row) {
            if ($row['score_count'] >= 4000):
                echo "<tr>";
                echo "<td><a target='_blank' href='" . $row["url"] . "'><p>" . $row["name"] . "</p></a></td>";
                echo "<td><p>" . $row["score"] . "</p></td>";
                echo "<td><p>" . $row["score_count"] . "</p></td>";
                echo "<td><p>" . $row["country"] . "</p></td>";
                echo "<td><p>" . $row["region"] . "</p></td>";
                echo "<td><p>" . $row["region_style"] . "</p></td>";
                echo "<td><p>" . $row["wineyard"] . "</p></td>";
                echo "<td><p>" . $row["alc"] . "</p></td>";
                echo "<td><p>" . $row["grape"] . "</p></td>";
                echo "<td><p>" . $row["price"] . "</p></td>";
                echo "</tr>";
            endif;
        }
        echo "</tbody></table>";
    }

    public function show_table_prepared()
    {
        $query = "SELECT * FROM wine_v1 ORDER BY score DESC, score DESC";
        $rows = $this->link->query($query);

        echo "<table class='table table-striped'><thead>
        <th>Name</th>
        <th>Score</th>
        <th>Reviews</th>
        <th>Country</th>
        <th>Country (numeric)</th>    
        <th>Region</th>
        <th>Region (numeric)</th>
        <th>Grapes</th>
        <th>Grapes (numeric)</th>
        <th>Type (numeric)</th>
        <th>Price ($)</th>
        ";
        echo "</thead><tbody>";
        foreach ($rows as $row) {
            if ($row['score_count'] >= 3000):
                echo "<tr>";
                echo "<td><a target='_blank' href='" . $row["url"] . "'><p>" . $row["name"] . "</p></a></td>";
                echo "<td><p>" . $row["score"] . "</p></td>";
                echo "<td><p>" . $row["score_count"] . "</p></td>";
                echo "<td><p>" . $row["country"] . "</p></td>";
                echo "<td><p>" . $row["country_numerical"] . "</p></td>";
                echo "<td><p>" . $row["region"] . "</p></td>";
                echo "<td><p>" . $row["region_numerical"] . "</p></td>";
                echo "<td><p>" . $row["grape"] . "</p></td>";
                echo "<td><p>" . $row["grape_numerical"] . "</p></td>";
                echo "<td><p>" . $row["type_numerical"] . "</p></td>";
                echo "<td><p>" . $row["price"] . "</p></td>";
                echo "</tr>";
            endif;
        }
        echo "</tbody></table>";
    }

    public function select_wine()
    {
        $query = "SELECT * FROM wine_v1 ORDER BY price DESC";
        $rows = $this->link->query($query); ?>
        <select id="select-wine">
            <option value="default">Select wine</option>
            <?php foreach ($rows as $row): ?>
                <?php if ($row['price']): ?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"] . ' (' . $row["price"] . '$)'; ?></option>
                <?php endif; ?>
            <?php endforeach; ?>
        </select>
        <?php
    }

    public function similar_wines($wine_ID)
    {
        //Collecting all wine attributes for the selected wine
        $query = "SELECT * FROM wine_v1 WHERE id ={$wine_ID}";
        $wine_rows = $this->link->query($query);

        $selected_wine = array();

        foreach ($wine_rows as $wine_row):
            $selected_wine = $wine_row;
        endforeach; ?>


        <?php //Creating an array with all wines
        $query = "SELECT * FROM wine_v1";
        $rows = $this->link->query($query);

        $compared_wines = array();
        foreach ($rows as $row):
            if (isset($row['id'])
                && isset($row['name'])
                && isset($row['country_numerical'])
                && isset($row['region_numerical'])
                && isset($row['grape_numerical'])
                && isset($row['type_numerical'])
                && isset($row['price'])
                && ($row['score_count'] >= 50)
            ):
                $compared_wines[] = $row;
            endif;
        endforeach;

        /* echo '</pre>';
         print_r($compared_wines);
         echo '</pre>';*/

        /*
         * Adding joint values between selected wine and compared wine.
         * If they have a joined value an 1 is added
         */

        $key = 0;
        foreach ($compared_wines as $compared_wine):

            //Setting the joint value to 0, before looping through a new wine
            $joint_value = 0;

            if ($selected_wine['country_numerical'] == $compared_wine['country_numerical']):
                $joint_value += 1;
            endif;

            if ($selected_wine['region_numerical'] == $compared_wine['region_numerical']):
                $joint_value += 1;
            endif;

            if ($selected_wine['grape_numerical'] == $compared_wine['grape_numerical']):
                $joint_value += 1;
            endif;

            if ($selected_wine['type_numerical'] == $compared_wine['type_numerical']):
                $joint_value += 1;
            endif;

            $compared_wines[$key]['joint_value'] = $joint_value;
            $compared_wines[$key]['unique_value'] = 4;
            $key++;
        endforeach;

        /*
         * Adding Jaccard's coefficient
         * */

        $key = 0;
        foreach ($compared_wines as $compared_wine):
            $coefficient = $compared_wine['joint_value'] / $compared_wine['unique_value'];
            $compared_wines[$key]['coefficient'] = $coefficient;
            $key++;
        endforeach;

        //Sorts the array
        foreach ($compared_wines as $key => $row) {
            $vc_array_coefficient[$key] = $row['coefficient'];
            $vc_array_score[$key] = $row['score'];
        }
        array_multisort($vc_array_coefficient, SORT_DESC, $vc_array_score, SORT_DESC, $compared_wines);

        /*
         * Displaying all results with coefficients
         */
        ?>
        <h3 class="similar-wine">Selected wine</h3>
        <?php $flag = false; ?>
        <?php $i = null; ?>
        <?php $wine = $selected_wine; ?>
        <div class="row align-row">
            <div class="wine-wrapper col-sm-3 text-center">
                <?php include('wine-teaser.php'); ?>
            </div>
        </div>
        <hr>
        <h3 class="recommendation">Recommendations based on Jaccard</h3>
        <?php $i = 1; ?>
        <?php $flag = true; ?>
        <?php foreach ($compared_wines as $compared_wine): ?>
        <?php if ($i <= 12): ?>
            <?php ;
            similar_text($selected_wine['name'], $compared_wine['name'], $percent);
            if ($percent <= 75):?>
                <?php $wine = $compared_wine; ?>
                <div class="wine-wrapper col-sm-3">
                    <?php include('wine-teaser.php'); ?>
                </div>
                <?php $i++; ?>
            <?php endif; ?>
        <?php endif; ?>
    <?php endforeach;
    }

    function star_displayed($score)
    {
        $score = round($score);

        for ($x = 1; $x <= $score; $x++) { ?>
            <img src="img/star-success.svg">
        <?php } ?>
        <?php $remainingStars = 5 - $score;
        for ($x = 1; $x <= $remainingStars; $x++) { ?>
            <img src="img/star-unsuccess.svg">
        <?php }
    }

    function shuffleBg()
    {
        $bg_colors = array('rgb(220, 215, 191)',
            'rgb(212, 197, 187)',
            'rgb(200, 209, 206)',
            'rgb(221, 185, 160)',
            'rgb(223, 218, 211)',
            'rgb(188, 188, 176)',
            'rgb(243, 227, 222)',
            'rgb(205, 200, 194)',
            'rgb(206, 183, 183)',
            'rgb(231, 220, 219)',
            'rgb(205, 200, 194)',
        );
        shuffle($bg_colors);
        return $bg_colors[0];
    }
}