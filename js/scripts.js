jQuery(function ($) {
    "use strict";
    $(document).ready(function () {

        $(".fade-in-child, .fade-in-left, .fade-in-up").waypoint({
            handler: function (direction) {
                if (direction == 'down') {
                    var element = this.element;
                    $(element).addClass("visible-fade");
                }
            },
            offset: '90%'
        });

        /************************
         * Insert URL
         ************************/
        var enable_url = true;
        $('.url').click(function (e) {
            if (enable_url) {
                var urls = $('#text-area-url').val();
                var type = $(this).attr('data-type');

                if (!urls.length) {
                    $('.response-message').html('Oops. You need to paste in a link.');
                }

                else {
                    const data = {};
                    data['type'] = type;
                    data['urls'] = urls;
                    ajax_url(data);
                }
            } else {
                $('.response-message').html('Please wait. Ajax is running.');
            }
        });

        function ajax_url(data) {
            $.ajax({
                url: 'ajax.php',
                type: 'post',
                data: {
                    data: data
                },
                beforeSend: function (e) {
                    enable_url = false;
                    $('.load-outer-wrapper-url').addClass('loading-posts');
                },
                success: function (result) {
                    console.log(result);

                    //Success.
                    if (result.search("Success") !== -1) {
                        $('.response-message').html(result);
                    }
                    else {
                        $('.response-message').html(result);
                    }
                },
                complete: function (e) {
                    enable_url = true;
                    $('.load-outer-wrapper-url').removeClass('loading-posts');
                }
            });
        }

        /************************
         * Explore related
         ************************/
        $('#select-wine').on('change', function () {
            var wine_ID = this.value;
            const data = {};
            data['type'] = 'selectWine';
            data['wine_ID'] = wine_ID;
            ajax_similarity(data);
        });

        function ajax_similarity(data) {
            $.ajax({
                url: 'ajax_similar.php',
                type: 'post',
                data: {
                    data: data
                },
                beforeSend: function (e) {
                    $('body').removeClass('wine-search');
                },
                success: function (result) {
                    console.log(result);
                    $("#similar-wines").html(result);
                },
                complete: function (e) {

                    var menuState = true;
                    var active = menuItemsFade(menuState);

                    if (active) {
                        setTimeout(function () {
                            $('body').addClass('wine-search');

                            $(".fill-img").each(function (index, element) {
                                var fill = $(this).attr('data-fill');
                                var fill_inset = "inset(" + fill + " 0 0 0)";
                                $(this).find('svg').css({
                                    "clip-path": fill_inset,
                                    "-webkit-clip-path": fill_inset
                                });
                            });
                        }, 2500);
                    }
                }
            });
        }

        function menuItemsFade(menuState) {
            if (menuState == true) {
                var menuItems = $("#similar-wines .wine-wrapper").length;
                var i = 0;
                var menuItemsShowInterval = setInterval(function () {
                    $("#similar-wines .wine-wrapper").eq(i).addClass("menu-item-show");
                    if (i == menuItems) {
                        clearInterval(menuItemsShowInterval);
                    }
                    i++;
                }, 100);
                return true;
            } else {
                $("#similar-wines .wine-wrapper").removeClass("menu-item-show");
            }
        }

        /************************
         * Add sitemap
         ************************/
        var enable_sitemap = true;
        $('.sitemap').click(function (e) {
            if (enable_sitemap) {
                var urls = $('#text-area-sitemap').val();
                var type = $(this).attr('data-type');

                if (!urls.length) {
                    $('.response-message-sitemap').html('Oops. You need to paste in a sitemap.');
                }
                else {
                    const data = {};
                    data['type'] = type;
                    data['urls'] = urls;
                    ajax_sitemap(data);
                }
            }
            else {
                $('.response-message-sitemap').html('Please wait. The server is executing AJAX.');
            }
        });

        function ajax_sitemap(data) {
            $.ajax({
                url: 'ajax.php',
                type: 'post',
                data: {
                    data: data
                },
                beforeSend: function (e) {
                    enable_sitemap = false;
                    $('.load-outer-wrapper-sitemap').addClass('loading-posts');
                },
                success: function (result) {
                    console.log(result);

                    //Success.
                    if (result.search("Success") !== -1) {
                        $('.response-message-sitemap').html(result);
                    }
                    else {
                        $('.response-message-sitemap').html(result);
                    }
                },
                complete: function (result) {
                    enable_sitemap = true;
                    $('.load-outer-wrapper-sitemap').removeClass('loading-posts');
                }
            });
        }
    });
});