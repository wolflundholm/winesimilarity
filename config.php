<?php

class config
{
    protected $db_conn;
    public $host = 'localhost';
    public $db_user = 'root';
    public $db_password = 'root';
    public $db_name = 'winescraper';

    public function dbConnection()
    {
        try {
            $this->db_conn = new PDO ("mysql:host=$this->host;dbname=$this->db_name", $this->db_user, $this->db_password);
            $this->db_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->db_conn;
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }
}