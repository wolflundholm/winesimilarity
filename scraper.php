<?php

require_once('config.php');
require_once('library/simple_html_dom.php');

class scrapeWine
{
    public $link;

    public function __construct()
    {
        $db_connection = new config();
        $this->link = $db_connection->dbConnection();
        return $this->link;
    }

    function getStringBetween($str, $from, $to)
    {
        $sub = substr($str, strpos($str, $from) + strlen($from), strlen($str));
        if ($sub):
            $sub = 'https://' . $sub;
            return substr($sub, 0, strpos($sub, $to));
        else:
            return null;
        endif;
    }

    public function insertWine($wine_values)
    {
        //Name is unique identifier
        $name = (isset($wine_values['name'])) ? $wine_values['name'] : null;

        $stmt = $this->link->prepare('SELECT name FROM wine_v1 WHERE name=?');
        $value = array($name);
        $stmt->execute($value);

        $count = $stmt->rowCount();
        if ($count !== 0) {
            echo $count . ' wine already exists in the database. Please try another link.';
        } else {
            $country = (isset($wine_values['country'])) ? $wine_values['country'] : null;
            $region = (isset($wine_values['region'])) ? $wine_values['region'] : null;
            $region_style = (isset($wine_values['region_style'])) ? $wine_values['region_style'] : null;
            $wineyard = (isset($wine_values['wineyard'])) ? $wine_values['wineyard'] : null;
            $alc = (isset($wine_values['alc'])) ? $wine_values['alc'] : null;
            $grape = (isset($wine_values['grape'])) ? $wine_values['grape'] : null;
            $score = (isset($wine_values['score'])) ? $wine_values['score'] : null;
            $score_count = (isset($wine_values['score_count'])) ? $wine_values['score_count'] : null;
            $price = (isset($wine_values['price'])) ? $wine_values['price'] : null;

            $lid = (isset($wine_values['lid'])) ? $wine_values['lid'] : null;
            $url = (isset($wine_values['url'])) ? $wine_values['url'] : null;

            $query = $this->link->prepare("INSERT INTO wine_v1 (name, country, region, region_style, wineyard, alc, grape, score, score_count, price, lid, url) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $values = array(self::uft8_encoding($name), self::uft8_encoding($country), self::uft8_encoding($region), self::uft8_encoding($region_style), self::uft8_encoding($wineyard), $alc, self::uft8_encoding($grape), $score, $score_count, $price, self::uft8_encoding($lid), self::uft8_encoding($url));
            $query->execute($values);
            $count = $query->rowCount();
            echo 'Success. ' . $count . ' wines added.';
        }
    }

    function uft8_encoding($string)
    {
        $string = transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove',
            $string);
        return $string;
    }

    function scrape_wine_url($urls)
    {
        foreach ($urls as $url):
            $html = file_get_html($url);

            $name = $html->find('span.wine-page__header__information__details__name__vintage', 0);

            if (isset($name)):
                $name = $name->plaintext;
                $country = $html->find('.wine-page__header__information__details__location__country', 0);
                if ($country):
                    $country = $country->plaintext;
                endif;
                $region = $html->find('.wine-region', 0)->plaintext;
                $wineyard = $html->find('.wine-page__summary__item__content a', 0)->plaintext;
                $alc = $html->find('.wine-page__summary .wine-page__summary__item .wine-page__summary__item__content', 5);
                if ($alc):
                    $alc = $alc->plaintext;
                    $alc = str_replace("%", "", $alc);
                    $alc = floatval(str_replace(",", ".", $alc));
                endif;

                $lid = $html->find('.wine-page__summary .wine-page__summary__item .wine-page__summary__item__content', 6);
                if ($lid):
                    $lid = $lid->plaintext;
                endif;
                $region_style = $html->find('.wine-page__summary .wine-page__summary__item .wine-page__summary__item__content', 3);
                if ($region_style):
                    $region_style = $region_style->plaintext;
                endif;
                $grape = $html->find('.wine-page__summary .wine-page__summary__item .wine-page__summary__item__content', 1);
                if ($grape):
                    //Format grape
                    $grape = $grape->plaintext;
                    $grape = str_replace(' ·', ',', $grape);
                endif;

                $price = $html->find('.wine-page__header__information__details__average-price__label--highlight', 0);
                if ($price):
                    $price = $price->plaintext;
                    $price = floatval(preg_replace("/[^-0-9\.]/", "", $price));
                    $price = round($price);
                endif;

                $score = $html->find('.wine-page__header__information__details__average-rating__value__number', 0);
                if ($score):
                    $score = $score->plaintext;
                endif;

                $score_count = $html->find('.wine-page__header__information__details__average-rating__label', 0);
                if ($score_count):
                    $score_count = filter_var($score_count->plaintext, FILTER_SANITIZE_NUMBER_INT);
                endif;

                $wine_values = array(
                    'name' => $name,
                    'country' => $country,
                    'region' => $region,
                    'region_style' => $region_style,
                    'wineyard' => $wineyard,
                    'alc' => $alc,
                    'lid' => $lid,
                    'grape' => $grape,
                    'score' => $score,
                    'score_count' => $score_count,
                    'price' => $price,
                    'url' => $url
                );
                self::insertWine($wine_values);
            else:
                echo 'Oops. You need to find a specific wine from vivino.com';
            endif;
        endforeach;
    }

    function get_xml_from_url($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        $xmlstr = curl_exec($ch);
        curl_close($ch);

        return $xmlstr;
    }
}

