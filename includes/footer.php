<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="footer-text">
                    All rights reserved: Mads Wolf Lundholm & Kasper Jensen (Copenhagen Business School).
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Scripts  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script><?php include_once('js/waypoints.min.js'); ?></script>
<script><?php include_once('js/scripts.js'); ?></script>
</body>
</html>