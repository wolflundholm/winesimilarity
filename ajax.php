<?php
require_once('scraper.php');
require_once('table.php');
$scrapeWine = new scrapeWine();
$table = new table();

if ($_POST['data']['type'] == 'sitemap'):
    $xmlstr = $scrapeWine->get_xml_from_url($_POST['data']['urls']);
    if ($xmlstr):
        $xmlobj = new SimpleXMLElement($xmlstr);
        $xmlobj = (array)$xmlobj;

        $urls = $xmlobj['url'];

        $i = 0;
        foreach ($urls as $url):
            $url_array[] = (string)$url->loc;
            $i += 1;
        endforeach;
        echo 'Counted urls in sitemap: ' . $i;

        $scrapeWine->scrape_wine_url($url_array);
    endif;
else:
    $urls = explode(" ", $_POST['data']['urls']);
    $scrapeWine->scrape_wine_url($urls);
endif;